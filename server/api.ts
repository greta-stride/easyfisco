// export const BASE_URL: String = "http://localhost:3005"
export const BASE_URL: String =
  "https://my-json-server.typicode.com/greta-calamari/landing-page-backend"

const FEATURES_ENDPOINT: String = "features"
export const API_FEATURES = `${BASE_URL}/${FEATURES_ENDPOINT}`

const PRICING_ENDPOINT: String = "pricing"
export const API_PRICING = `${BASE_URL}/${PRICING_ENDPOINT}`

const FAQ_ENDPOINT: String = "faq"
export const API_FAQ = `${BASE_URL}/${FAQ_ENDPOINT}`
