export async function send(
  to_name: string,
  from_name: string,
  email: string,
  phoneNumber: string,
  coupon: string,
  hasPartitaIva: string
) {
  const data = {
    service_id: "service_iyu8rvf",
    template_id: "template_1doyugt",
    user_id: "43zGISgHIPuBrw2I1",
    template_params: {
      from_name: from_name,
      to_name: to_name,
      phoneNumber: phoneNumber,
      email: email,
      coupon: coupon,
      hasPartitaIva: hasPartitaIva,
    },
  }

  const res = await fetch("https://api.emailjs.com/api/v1.0/email/send", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-Type": "application/json" },
  })
  if ((await res.text()) === "OK") {
    return res
  } else {
    throw new Error("error sending email")
  }
}
