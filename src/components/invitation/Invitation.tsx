import Link from "next/link"

const Invitation: React.FC = () => {
  return (
    <>
      <div className="basic-3">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h4>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Asperiores, eligendi.
              </h4>
              <Link href="">
                <p className="btn-outline-lg page-scroll">Contattaci</p>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Invitation
