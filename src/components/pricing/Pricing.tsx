import Image from "next/image"
import { Price } from "../../../types"
import Link from "next/link"

interface PricingProps {
  items: Price[]
}

const Pricing: React.FC<PricingProps> = ({ items }) => {
  return (
    <>
      <div className="cards-2">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h2 className="heading">Un piano per ogni</h2>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              {items &&
                items.map((item: Price) => {
                  return (
                    <div className="card" key={item.id.toString()}>
                      <div className="card-body">
                        <div className="card-title">
                          <Image
                            alt="alternative"
                            src="/images/decoration-lines.svg"
                            className="image-fluid decoration-lines"
                            width={30}
                            height={9.75}
                          />

                          <span>{item.name}</span>

                          <Image
                            alt="alternative"
                            src="/images/decoration-lines.svg"
                            className="image-fluid decoration-lines flipped"
                            width={30}
                            height={9.75}
                          />
                        </div>
                        <ul className="list-unstyled list-space-lg">
                          {item.features.map(
                            (feature: String, index: Number) => {
                              return (
                                <li key={`${feature}-${index}`}>{feature}</li>
                              )
                            }
                          )}
                        </ul>

                        <div className="price">
                          {item.value}
                          <span>/anno</span>
                        </div>
                        <Link href="/contact">
                          <p className="btn-solid-reg">Contattaci</p>
                        </Link>
                      </div>
                    </div>
                  )
                })}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Pricing
