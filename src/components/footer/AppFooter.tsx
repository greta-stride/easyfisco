import { NextPage } from "next"
import Link from "next/link"

const AppFooter: React.FC = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="footer-col first">
              <h6>Fisco</h6>
              <p>Inizia subito</p>
            </div>
            <div className="footer-col second">
              <h6>Link utili</h6>
              <ul className="list-unstyled li-space-lg p-small">
                <li>IMP</li>
                <li>GUIDE</li>
                <li>
                  Supporto:
                  <Link href={"/contact"}>
                    <p>Contattaci</p>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="footer-col third">
              <span className="fa-stack">
                <a href="">
                  <i className="fas fa-circle fa-stack-2x"></i>
                  <i className="fab fa-facebook fa-stack-1x"></i>
                </a>
              </span>

              <span className="fa-stack">
                <a href="">
                  <i className="fas fa-circle fa-stack-2x"></i>
                  <i className="fab fa-twitter fa-stack-1x"></i>
                </a>
              </span>

              <span className="fa-stack">
                <a href="">
                  <i className="fas fa-circle fa-stack-2x"></i>
                  <i className="fab fa-pinterest-p fa-stack-1x"></i>
                </a>
              </span>

              <span className="fa-stack">
                <a href="">
                  <i className="fas fa-circle fa-stack-2x"></i>
                  <i className="fab fa-instagram fa-stack-1x"></i>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default AppFooter
