import Image from "next/image"

const Header: React.FC = () => {
  return (
    <div>
      <header className="header">
        <div className="container">
          <div className="row">
            <div className="col-lg-5">
              <div className="text-container mt-5">
                <h1 className="h1-large">
                  La moneta più importante che abbiamo
                  <span> è il tempo</span>
                </h1>
                <p className="p-large">
                  EasyFisco gestisce i clienti e la fiscalità per te , per darti
                  modo di dedicare il tuo tempo alle cose più importanti
                </p>
                <button className="btn-solid-lg">Dimmi di più</button>
              </div>
            </div>
            <div className="col-lg-6">
              <Image
                src="/images/header.svg"
                className="img-fluid"
                height={500}
                width={500}
                alt="EasyFisco"
                priority
              />
            </div>
          </div>
        </div>
      </header>
    </div>
  )
}

export default Header
