import Image from "next/image"

const Details: React.FC = () => {
  return (
    <>
      <div className="basic-1">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-xl-5">
              <div className="text-container">
                <h2>Lorem ipsum dolor sit amet.</h2>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Fugit, dolores?
                </p>
                <a className="btn-solid-reg" href="#picing">
                  Prezzi
                </a>
              </div>
            </div>

            <div className="col-lg-6 col-xl-7">
              <div className="image-container">
                <Image
                  alt="alternative"
                  src="/images/detail-1.svg"
                  className="image-fluid"
                  width={500}
                  height={400}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="basic-2">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-xl-7">
              <div className="image-container">
                <Image
                  alt="alternative"
                  src="/images/detail-2.svg"
                  className="image-fluid"
                  width={500}
                  height={400}
                />
              </div>
            </div>
            <div className="col-lg-6 col-xl-5">
              <div className="text-container">
                <h2>Lorem ipsum dolor sit amet.</h2>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Fugit, dolores?
                </p>
                <ul className="list-unstyled li-space-lg">
                  <li className="d-flex">
                    <i className="fas fa-square"></i>
                    <div className="flex-grow-1">
                      Organizza Lorem ipsum dolor sit amet.
                    </div>
                  </li>
                  <li className="d-flex">
                    <i className="fas fa-square"></i>
                    <div className="flex-grow-1">
                      Organizza Lorem ipsum dolor sit amet.
                    </div>
                  </li>
                  <li className="d-flex">
                    <i className="fas fa-square"></i>
                    <div className="flex-grow-1">
                      Organizza Lorem ipsum dolor sit amet.
                    </div>
                  </li>
                </ul>
                <a className="btn-solid-reg" href="#pricing">
                  Prezzi
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Details
