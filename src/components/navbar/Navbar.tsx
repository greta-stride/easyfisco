import Link from "next/link"

const Navbar: React.FC = () => {
  return (
    <>
      <nav
        id="Navbar"
        className="navbar navbar-expand-lg fixed-top navbar-light"
        aria-label="Main navigation"
      >
        <div className="container">
          <Link href="/">
            <p className="navbar-brand logo-text">EasyFisco</p>
          </Link>

          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div
            className="navbar-collapse offcanvas-collapse"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav ms-auto navbar-nav-scroll">
              <li className="nav-item">
                <Link href="/" className="navLink">
                  <p className="nav-link">Home</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/#features" className="navLink">
                  <p className="nav-link">Funzionalità</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/#details" className="navLink">
                  <p className="nav-link">Dettagli</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/#prices" className="navLink">
                  <p className="nav-link">Prezzi</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="navLink"
                  href="/contact
                "
                >
                  <p className="nav-link">Contattaci</p>
                </Link>
              </li>
            </ul>

            <span>
              <a href="#" className="btn-outline-sm">
                Accedi
              </a>
            </span>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar
