import { ReactNode } from "react"
import AppFooter from "../components/footer/AppFooter"
import Navbar from "../components/navbar/Navbar"

interface LayoutProps {
  children: ReactNode
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Navbar />
      {children}
      <AppFooter />
    </>
  )
}

export default Layout
