import Layout from "@/layouts/Layout"
import { NextPage } from "next"
import Image from "next/image"
import { useState } from "react"
import { ContactForm } from "../../types"
import { send } from "../../services/emailjs.service"
import Head from "next/head"

const Contact: NextPage = () => {
  const [form, setForm] = useState<ContactForm>({
    fullName: "",
    email: "",
    phoneNumber: "",
    coupon: "",
    hasPartitaIva: "",
    privacy: false,
  })

  const [privacy, setPrivacy] = useState<boolean>(false)

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()
    const { fullName, email, phoneNumber, coupon, hasPartitaIva } = form
    send("Cristian", fullName, email, phoneNumber, coupon || "", hasPartitaIva)
      .then(() => {
        console.log("Email inviata!")
      })
      .catch((error) => {
        console.error(error)
      })
  }

  function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    setForm({
      ...form,
      [event.currentTarget.name]: event.currentTarget.value,
    })
  }
  return (
    <Layout>
      <Head>
        <title>EasyFisco - Contatti</title>
        <meta
          name="description"
          content="EasyFisco gestisce i clienti e la fiscalità per te, per darti modo di dedicare il tuo tempo alle cose più importanti"
        />
      </Head>
      <header className="header">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-5">
              <div className="text-container mt-5">
                <h1 className="h1-large">Vuoi saperne di più?</h1>
                <h1 className="h1-orange">Contattaci</h1>
                <p className="p-large">
                  Siamo disponibili 24/7 per rispondere ad ogni tua domanda
                </p>
                <div>
                  <a className="btn-solid-lg" href="#contact-form">
                    Vai al form
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <Image
                src="/images/contact-us.svg"
                className="img-fluid"
                height={500}
                width={500}
                alt="EasyFisco"
                priority
              />
            </div>
          </div>
        </div>
      </header>

      <div id="contact" className="contact">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="h2-heading">
                <h2>
                  Compila il form{" "}
                  <span className="span-contacts">per essere contattato</span>
                </h2>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <form onSubmit={handleSubmit}>
                <div className="row">
                  <div className="col mb-4 text-start">
                    <label htmlFor="fullName" className="form-label">
                      Nome e Cognome *
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="fullName"
                      name="fullName"
                      placeholder="Es. Mario Rossi"
                      aria-label="Nome e Cognome"
                      required
                      value={form.fullName}
                      onChange={handleChange}
                    />
                  </div>

                  <div className="col mb-4 text-start">
                    <label htmlFor="email" className="form-label">
                      Email *
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      placeholder="Es. mario_rossi@gmail.com"
                      aria-label="Email"
                      required
                      value={form.email}
                      onChange={handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-12">
                    <div className="row">
                      <div className="col mb-4 text-start">
                        <label htmlFor="phoneNumber" className="form-label">
                          Telefono *
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="phoneNumber"
                          name="phoneNumber"
                          placeholder="Es. 333"
                          aria-label="telefono"
                          required
                          value={form.phoneNumber}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col mb-4 text-start">
                        <label htmlFor="coupon" className="form-label">
                          Coupon
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="coupon"
                          name="coupon"
                          placeholder="Coupon"
                          aria-label="coupon"
                          value={form.coupon}
                          onChange={handleChange}
                        />
                      </div>
                    </div>

                    <div className="form-check text-start">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="hasPartitaIva"
                        id="noPartitaIva"
                        value="no"
                        onChange={handleChange}
                        checked={form.hasPartitaIva === "no"}
                      />
                      <label
                        className="form-check-label"
                        htmlFor="noPartitaIva"
                      >
                        Non ho una partita IVA e vorrei aprirne una
                      </label>
                    </div>
                  </div>
                </div>

                <div className="form-check text-start">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="hasPartitaIva"
                    id="siPartitaIva"
                    value="si"
                    onChange={handleChange}
                    checked={form.hasPartitaIva === "si"}
                  />
                  <label className="form-check-label" htmlFor="siPartitaIva">
                    Ho già una partita IVA e vorrei passare a EasyFisco
                  </label>
                </div>

                <div className="mt-4 form-check text-start">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    value="privacy"
                    name="privacy"
                    checked={privacy}
                    required
                    onChange={() => setPrivacy(!privacy)}
                  />
                  <label className="form-check-label" htmlFor="privacy">
                    Ho letto e accetto l'informativa sulla Privacy
                  </label>
                </div>

                <button type="submit" className="btn-solid-lg">
                  INVIA
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
export default Contact
