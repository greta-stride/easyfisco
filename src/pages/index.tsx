import Header from "@/components/header/Header"
import Layout from "@/layouts/Layout"
import { API_FAQ, API_FEATURES, API_PRICING } from "../../server/api"
import Features from "@/components/features/Features"
import { Feature, Price, Question } from "../../types"
import { NextPage } from "next"
import Details from "@/components/details/Details"
import Invitation from "@/components/invitation/Invitation"
import Pricing from "@/components/pricing/Pricing"
import Faq from "@/components/faqs/Faq"
import Head from "next/head"
type HomeProps = {
  features: Feature[]
  pricing: Price[]
  questions: Question[]
}

const Home: NextPage<HomeProps> = ({ features, pricing, questions }) => {
  return (
    <div>
      <Head>
        <title>EasyFisco</title>
        <meta
          name="description"
          content="EasyFisco gestisce i clienti e la fiscalità per te, per darti modo di dedicare il tuo tempo alle cose più importanti"
        />
      </Head>
      <Layout>
        <Header />
        <Features items={features} />
        <Details />
        <Invitation />
        <Pricing items={pricing} />
        <Faq items={questions} />
      </Layout>
    </div>
  )
}
export default Home

export async function getStaticProps() {
  try {
    const resFeature = await fetch(API_FEATURES)
    const features = await resFeature.json()

    const resPricing = await fetch(API_PRICING)
    const pricing = await resPricing.json()

    const resFaq = await fetch(API_FAQ)
    const questions = await resFaq.json()

    if (!resFeature.ok || !resPricing.ok || !resFaq.ok) {
      return {
        notFound: true,
      }
    }

    return {
      props: { features, pricing, questions },
    }
  } catch (error) {
    return {
      notFound: true,
    }
  }
}
